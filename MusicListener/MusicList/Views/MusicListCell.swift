//
//  MusicListCell.swift
//  MusicListener
//
//  Created by Felipe Jerez on 09-03-20.
//  Copyright © 2020 felipejerez. All rights reserved.
//

import UIKit

class MusicListCell: UITableViewCell {

    
    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var albumName: UILabel!
    @IBOutlet weak var songName: UILabel!
    
    var loader = ImageLoader()
    var onReuse: () -> Void = {}
    
    func setMusicInfo(artist : Artist){
        //print(artist)
        artistName.text = artist.artistName
        albumName.text = artist.collectionName
        songName.text = artist.trackName
        
        let url = URL(string: artist.artworkUrl100)!
        let token = loader.loadImage(url) { result in
          do {
            let image = try result.get()
            DispatchQueue.main.async {
                self.albumImage.image = image
            }
          } catch {
            print(error)
          }
        }
        onReuse = {
            if let token = token {
                self.loader.cancelLoad(token)
            }
        }

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
      super.prepareForReuse()
      onReuse()
      albumImage.image = nil
    }
    
}
