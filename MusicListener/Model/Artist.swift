//
//  Artist.swift
//  MusicListener
//
//  Created by Felipe Jerez on 09-03-20.
//  Copyright © 2020 felipejerez. All rights reserved.
//

import Foundation

struct Artist: Codable{
    let artistID : Int?
    let artistName : String?;
    let collectionName : String?;
    let trackName : String?;
    var previewUrl : String?;
    let artworkUrl100 : String;
    let trackPrice : Double?;
    let currency : String?;
    let releaseDate : String?;
}
