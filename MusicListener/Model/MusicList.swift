//
//  MusicList.swift
//  MusicListener
//
//  Created by Felipe Jerez on 10-03-20.
//  Copyright © 2020 felipejerez. All rights reserved.
//

import Foundation

struct MusicListResponse: Codable{
    let resultCount : Int
    let results : [Artist]
}
