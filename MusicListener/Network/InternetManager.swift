//
//  InternetManager.swift
//  MusicListener
//
//  Created by Felipe Jerez on 12-03-20.
//  Copyright © 2020 felipejerez. All rights reserved.
//

import Foundation
import Reachability

class InternetManager: NSObject {

    var reachability: Reachability!

    // Create a singleton instance
    static let sharedInstance: InternetManager = { return InternetManager() }()


    override init() {
        super.init()

        // Initialise reachability
        do {
            reachability = try Reachability()
        } catch{
            print("internet connection catch \(error)")
        }
        

        // Register an observer for the network status
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )

        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }

    @objc func networkStatusChanged(_ notification: Notification) {
        // Do something globally here!
    }

    static func stopNotifier() -> Void {
        do {
            // Stop the network status notifier
            try (InternetManager.sharedInstance.reachability).startNotifier()
        } catch {
            print("Error stopping notifier")
        }
    }

    // Network is reachable
    static func isReachable(completed: @escaping (InternetManager) -> Void) {
        if (InternetManager.sharedInstance.reachability).connection != .unavailable {
            completed(InternetManager.sharedInstance)
        }
    }

    // Network is unreachable
    static func isUnreachable(completed: @escaping (InternetManager) -> Void) {
        if (InternetManager.sharedInstance.reachability).connection == .unavailable {
            completed(InternetManager.sharedInstance)
        }
    }

    // Network is reachable via WWAN/Cellular
    static func isReachableViaWWAN(completed: @escaping (InternetManager) -> Void) {
        if (InternetManager.sharedInstance.reachability).connection == .cellular {
            completed(InternetManager.sharedInstance)
        }
    }

    // Network is reachable via WiFi
    static func isReachableViaWiFi(completed: @escaping (InternetManager) -> Void) {
        if (InternetManager.sharedInstance.reachability).connection == .wifi {
            completed(InternetManager.sharedInstance)
        }
    }
}
