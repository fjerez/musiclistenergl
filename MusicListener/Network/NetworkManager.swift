//
//  NetworkManager.swift
//  MusicListener
//
//  Created by Felipe Jerez on 10-03-20.
//  Copyright © 2020 felipejerez. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager{
    
    static let shared = NetworkManager()
    private init() {}
    
    func getMusicList(searchText : String, completionHandler: @escaping (MusicListResponse) -> Void ) {
        InternetManager.isReachable { instance in
          var result = MusicListResponse( resultCount: 0, results: [])
          AF.request("https://itunes.apple.com/search?term="+searchText.replacingOccurrences(of : " ", with: "+" )+"&mediaType=music&limit=20").response { response in
              if response.error == nil {
                      let decoder = JSONDecoder()
                      result = try! decoder.decode(MusicListResponse.self, from: response.data!)
                      //print("result")
                      //print(result)
                      completionHandler(result)
              } else {
                  completionHandler(result)
              }
          }
        }
    }
}
